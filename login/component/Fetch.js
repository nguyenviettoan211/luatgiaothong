import React from 'react';
import { FlatList, ActivityIndicator, Text, View,Alert  } from 'react-native';

export default class FetchExample extends React.Component {

    constructor(props){
        super(props);
        this.state ={ isLoading: true}
    }

    componentDidMount(){
        return fetch('https://facebook.github.io/react-native/movies.json')
            .then((response) => response.json())
            .then((responseJson) => {

                this.setState({
                    isLoading: false,
                    dataSource: responseJson.movies,
                    titles:responseJson.title,
                    description:responseJson.description,
                }, function(){

                });

            })
            .catch((error) =>{
                console.error(error);
            });
    }
    Press(){
        alert(this.state.description)
    }


    render(){

        if(this.state.isLoading){
            return(
                <View style={{flex: 1, padding: 20}}>
                    <ActivityIndicator/>
                </View>
            )
        }

        return(
            <View style={{flex: 1, paddingTop:20}}>
                <Text onPress={()=>{this.Press()}}>Press me</Text>
                <Text>{this.state.titles}</Text>
                <FlatList
                    data={this.state.dataSource}
                    renderItem={({item}) => <Text>{item.title}, {item.releaseYear}</Text>}
                    keyExtractor={(item, index) => index}
                />
            </View>
        );
    }
}
