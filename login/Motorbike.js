import React,{Component} from 'react';
import{TextInput,Text,View, Image,StyleSheet,TouchableOpacity, Alert, FlatList,TouchableHighlight} from 'react-native';
import {StackNavigator,DrawerNavigator} from 'react-navigation';
import {styles} from './styles';
export  default class Motorbike extends Component{
    render(){
        return(
            <View style={styles.headerMain}>
                <View style={{flex:1.7,flexDirection:'row', backgroundColor:'#EF4444'}}>
                    <View style={{flex:1, justifyContent:'center'}}>
                        <TouchableOpacity style={{height:40,width:40}} onPress={() => this.props.navigation.openDrawer()}>
                            <View style={{flex :1.1, padding:10}}>
                                <Image source={(require('./image/menuicon.png'))} style={{height:30,width:30}}/>
                            </View>
                        </TouchableOpacity>
                    </View>
                    <View style={{flex:1,justifyContent:'center',alignItems:'flex-end',padding:10}}>
                        <TouchableOpacity style={{height:40,width:40}} onPress={()=>{this.props.navigation.navigate('MainMenu')}}>
                            <View style={{flex:1, padding:10 }}>
                                <Image source={require('./image/Back.png')} style={{height:30,width:30}} />
                            </View>
                        </TouchableOpacity>
                    </View>
                </View>
                <View style={{flex:1.6,backgroundColor:'#455a64',flexDirection:'row'}}>
                    <View style={{flex:1,alignItems:'center',justifyContent:'center'}}>
                        <Text style={{color:'white'}}>
                            Tìm kiếm:
                        </Text>
                    </View>
                    <View style={{flex:4,alignItems:'center',justifyContent:'center'}}>
                        <TextInput  underlineColorAndroid={'transparent'} style={{fontSize:13,
                            color: 'gray',height:40,width:300, backgroundColor: 'white',borderRadius:5}}/>
                    </View>
                </View>
                <View style={{flex:18}}>
                    <View style={styles.TouchableView}>
                        <TouchableOpacity style={styles.Touchable} >
                            <View style={styles.TouchableView}>
                                <Text  style={{color:'white'}}>Den do</Text>
                            </View>
                        </TouchableOpacity>
                    </View>
                    <View style={styles.TouchableView}>
                        <TouchableOpacity style={styles.Touchable} >
                            <View style={styles.TouchableView}>
                                <Text  style={{color:'white'}}>Den do</Text>
                            </View>
                        </TouchableOpacity>
                    </View>
                    <View style={styles.TouchableView}>
                        <TouchableOpacity style={styles.Touchable} >
                            <View style={styles.TouchableView}>
                                <Text  style={{color:'white'}}>Den do</Text>
                            </View>
                        </TouchableOpacity>
                    </View>
                    <View style={styles.TouchableView}>
                        <TouchableOpacity style={styles.Touchable} >
                            <View style={styles.TouchableView}>
                                <Text  style={{color:'white'}}>Den do</Text>
                            </View>
                        </TouchableOpacity>
                    </View>
                    <View style={styles.TouchableView}>
                        <TouchableOpacity style={styles.Touchable} >
                            <View style={styles.TouchableView}>
                                <Text  style={{color:'white'}}>Den do</Text>
                            </View>
                        </TouchableOpacity>
                    </View>
                    <View style={styles.TouchableView}>
                        <TouchableOpacity style={styles.Touchable} >
                            <View style={styles.TouchableView}>
                                <Text  style={{color:'white'}}>Den do</Text>
                            </View>
                        </TouchableOpacity>
                    </View>
                    <View style={styles.TouchableView}>
                        <TouchableOpacity style={styles.Touchable} >
                            <View style={styles.TouchableView}>
                                <Text  style={{color:'white'}}>Den do</Text>
                            </View>
                        </TouchableOpacity>
                    </View>
                </View>
            </View>
        );
    }
}