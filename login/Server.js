import React, {Component} from 'react';
import {AppRegistry, SectionList, StyleSheet, Text, View, Alert, Platform} from 'react-native';
import {apiCall} from "./apiCall";

const apiGetAllFoods = 'http://localhost:3001/list_all_foods';
const apiInsertAccount = 'http://vnas.itrc.hanu.vn:2001/law/api/v1/users';

async function getFoodsFromServer() {
   try {
       let response = await fetch(apiGetAllFoods);
       let responseJson = await response.json();
       return responseJson.data; //list of foods
   } catch (error) {
       console.error(`Error is : ${error}`);
   }
}

//send post request to insert new data
export const insertAccount = async (params) => {
   const {response, error} = await apiCall({endPoint: apiInsertAccount, method: "POST", payload: params})
   return {response, error}
}
export {getFoodsFromServer};
// export {insertAccount};
